﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.IO;

namespace IRCServices
{
    class Program
    {
        public static Logger log = new Logger("services.log");
        private static StreamWriter writer;
        private static bool hasBurst;
        private static bool hasSentBurst;

        public static void Send(string message)
        {
            writer.WriteLine(message);
            writer.Flush();
            log.Log("> " + message);
            //Console.WriteLine("> " + message);
        }

        static void Main(string[] args)
        {
            // init log
            log.Log("Init...");
            
            NetworkStream stream;
            StreamReader reader;
            TcpClient IRC;
            string inputLine;
            try
            {
                log.Log("Connecting to " + Global.host + ":" + Global.port);
                IRC = new TcpClient(Global.host, Global.port);
                stream = IRC.GetStream();
                writer = new StreamWriter(stream);
                reader = new StreamReader(stream);
                while (true)
                {
                    while ((inputLine = reader.ReadLine()) != null)
                    {
                        log.Log("< " + inputLine);
                        log.LogDebug("< " + inputLine);
                        parseLine(inputLine);
                        if (!Global.AddServicesDone)
                        {
                            Functions.AddServices();
                        }
                    }
                }

            }
            catch (System.Net.Sockets.SocketException e)
            {
                log.Log("A socket exception occured, reconneting in 5 seconds.");
                log.Log("Exception: " + e.Message.ToString());
                System.Threading.Thread.Sleep(5000);
                Main(null);
            }

            catch (Exception e)
            {
                log.Log("An unexpected error occured.");
                log.Log(e.Message.ToString());
            }
        }

        public static void parseLine(string line)
        {
            string[] lineArray = line.Split(' ');
            if (!hasBurst)
            {
                burst();
                //Module.callHook(Hooks.SERVER_BURST_START, null, null);
            }
            string lineStart = lineArray[0];
            if (lineStart == "PING")
            {
                Send("PONG " + lineArray[1]);
                if (hasBurst && !hasSentBurst)
                {
                    Send(string.Format(":{0} EOB", Global.sid));
                    log.Log("Burst complete");
                    hasSentBurst = true;
                }
            }
        }

        public static void burst()
        {
            Send("PASS " + "HEQw7pMSiMPrQ" + " TS 6 :" + Global.sid);
            Send("CAPAB :UNKLN KLN ENCAP KNOCK GLN TBURST TB CHW IE EX CLUSTER EOB LL QS");
            Send("SERVER " + Global.servername + " 0 :(H) Network Service");
            Send("SVINFO 6 3 0 :" + Functions.getTimestamp());
            hasBurst = true;
            return;
        }
    }
}