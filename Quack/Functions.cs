﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IRCServices
{
    class Functions
    {
        public static string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        public static string GenerateUID()
        {
            return Global.sid + RandomString(6, true);
        }
        
        public static void IntroduceClient(string nickname, string username, string hostname, string gecos, string modes)
        {
            // :8HS UID nickname 1 timestamp +mode username hostname 0 uid 0 hostname :gecos
            Program.Send(string.Format(":8HS UID {0} 1 {1} +{2} {3} {4} 0 {5} 0 {6} :{7}", nickname, getTimestamp(), modes, username, hostname, "8HSAJ4KD8", hostname, gecos));
            Program.Send(string.Format("ENCAP * SVSMODE {0} {1} +o :1", nickname, getTimestamp()));
        }

        public static int getTimestamp()
        {
            TimeSpan t = (DateTime.UtcNow - new DateTime(1970, 1, 1));
            int UnixEpoch = (int)t.TotalSeconds;
            return UnixEpoch;
        }

        public static void AddServices()
        {
            IntroduceClient("PseudoServ", "PseudoServ", "services.network.com", "Network Service", "aiou");
            Global.AddServicesDone = true;
        }
    }
}
